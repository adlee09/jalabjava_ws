package urma.chap2;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Adam on 7/14/2015.
 */
public class SVCExample {
    private final static int var = 10; //must be effectively final
    public static void main(String[] args) {
        List<Integer> intSeq = Arrays.asList(1, 2, 3);
        intSeq.forEach(x -> System.out.println(x + var));
    }
}